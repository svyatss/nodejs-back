var MongoClient = require('mongodb').MongoClient;
var urlModule = require('url');

var rq = require('./RequestHandler.js');
var config = require('./config.js').config;

rq = new rq.RequestHandler();

var url = "mongodb://localhost/test";
var db;

MongoClient.connect(url, function(err, database){
    if(err) throw err;
    db = database;
    rq.app.listen(3000);
});

//products collection
rq.addHandler("get","/products", function(req, res){
    var cursor = db.collection(config.products_collection).find().toArray(function(err,data){
        for (var i = 0; i < data.length; i++){
            data[i].id = data[i]._id;
            delete data[i]._id;
        }
        res.send(data);
    });
});

rq.addHandler("post","/products", function(req,res){
    db.collection(config.products_collection).insertOne(
        req.body,
        function(err,result){
            res.send({newid:result.insertedId});
        }
    );
});

rq.addHandler("put","/products/:id", function(req, res){
    var id = rq.id(req.body.id);
    console.log(id);
    delete req.body.id;
    db.collection(config.products_collection).updateOne(
        {"_id":id},
        {
            $set: req.body
        }, function(err,result){
            console.log(result.result);
            res.send({});
        }
    );
});

rq.addHandler("delete","/products/:id", function(req, res){
    var id = rq.id(req.body.id);
    db.collection(config.products_collection).removeOne({"_id":id}, function(err,result){
        res.send({});
    });
});


//orders collection
rq.addHandler("get","/orders", function(req, res){
    var cursor = db.collection(config.orders_collection).find().toArray(function(err,data){
        for (var i = 0; i < data.length; i++){
            data[i].id = data[i]._id;
            delete data[i]._id;
        }
        res.send(data);

    });
});

rq.addHandler("post","/orders", function(req,res){
    db.collection(config.orders_collection).insertOne(
        req.body,
        function(err,result){
            res.send({newid:result.insertedId});
        }
    );
});

rq.addHandler("put","/orders/:id", function(req, res){
    var id = rq.id(req.body.id);
    console.log(id);
    delete req.body.id;
    db.collection(config.orders_collection).updateOne(
        {"_id":id},
        {
            $set: req.body
        }, function(err,result){
            console.log(result.result);
            res.send({});
        }
    );
});

rq.addHandler("delete","/orders/:id", function(req, res){
    var id = rq.id(req.body.id);
    db.collection(config.orders_collection).removeOne({"_id":id}, function(err,result){
        res.send({});
    });
});


//detalization

rq.addHandler("get", "/categories", function(req,res){
    var a = db.collection(config.products_collection).distinct('category', function(err, result){
        res.send(result);
    });
});

rq.addHandler("get", "/categories/:id", function(req,res){
    var category = urlModule.parse(req.url).pathname.split('/')[2];
    db.collection(config.products_collection).aggregate(
        [ {$match : { category : category }},
            { 
            $lookup:{
                from: config.orders_collection,
                localField: "code",
                foreignField:"specification",
                as: "joinData"
            }, },
            {$project: {data: "$joinData"}} ],
        function(err, result){
            var item = [];
            for (val of result){
                if(val.data[0]){
                    item.push(val.data[0]);
                }
                console.log(item);
            }
            res.send(item);
        }
    );
});