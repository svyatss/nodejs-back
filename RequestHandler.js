var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var ObjectId = require('mongodb').ObjectId;

exports.RequestHandler = function RequestHandler(){

    this.app = express();
    this.app.use(express.static(path.join(__dirname, 'public')));
    this.app.use(bodyParser());
    var self = this;

    this.addHandler = function(method, url, callback){
        self.app[method](url, callback);
    }

    this.id = function(id){
        return ObjectId(id);
    }
}

